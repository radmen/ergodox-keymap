mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
current_dir := $(patsubst %/,%,$(dir $(mkfile_path)))

.PHONY: keymaps
keymaps: qmk_firmware/keyboards/ergodox_ez/keymaps/radmen

qmk_firmware/keyboards/ergodox_ez/keymaps/radmen:
	ln -s $(current_dir)/src qmk_firmware/keyboards/ergodox_ez/keymaps/radmen
