qk_tap_dance_action_t tap_dance_actions[] = {
    [TD_LCBR] = ACTION_TAP_DANCE_HOLD(KC_LCBR, KC_LPRN),
    [TD_RCBR] = ACTION_TAP_DANCE_HOLD(KC_RCBR, KC_RPRN),
    [TD_LABK] = ACTION_TAP_DANCE_HOLD(KC_LABK, KC_LBRACKET),
    [TD_RABK] = ACTION_TAP_DANCE_HOLD(KC_RABK, KC_RBRACKET),
};
