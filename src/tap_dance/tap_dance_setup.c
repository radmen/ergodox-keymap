#include "tap_dance_setup.h"

void qk_tap_dance_hold_finished(qk_tap_dance_state_t *state, void *user_data) {
    qk_tap_dance_altgr_t *pair = (qk_tap_dance_altgr_t *)user_data;

    dprintf(
            "qk_tap_dance_hold_finished> c: %u, f: %u, i: %u p: %u \n",
            state->count,
            state->finished,
            state->interrupted,
            state->pressed
    );

    if (state->pressed) {
        tap_code16(pair->holdKc);
    } else {
        tap_code16(pair->kc);
    }
}
