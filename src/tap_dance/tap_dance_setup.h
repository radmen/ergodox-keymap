typedef struct {
  uint16_t kc;
  uint16_t holdKc;
} qk_tap_dance_altgr_t;

#define ACTION_TAP_DANCE_HOLD(kc, holdKc) \
  { .fn = {NULL, qk_tap_dance_hold_finished, NULL}, .user_data = (void *)&((qk_tap_dance_altgr_t){kc, holdKc}), }

void qk_tap_dance_hold_finished(qk_tap_dance_state_t *state, void *user_data);
